FROM eclipse-temurin:17-jre

ENV DB_HOST localhost
ENV DB_NAME db
ENV DB_USERNAME test
ENV DB_PASSWORD test

ARG JAR_FILE=build/libs/koshelok-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} app.jar

ENTRYPOINT ["java","-jar","-XX:+UseSerialGC","-Xss512k","-XX:MaxRAM=256m","/app.jar"]
