package sirius.tinkoff.koshelok

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.http.MediaType
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.test.web.servlet.ResultMatcher
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.request.RequestPostProcessor
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import sirius.tinkoff.koshelok.dto.*

val mockBalanceDto = BalanceDto(currencyDto = CurrencyDto(1, "RUB", "Российский рубль"), amount = "150")

val mockTransactionDto = TransactionDto(
    id = 1,
    balanceDto = mockBalanceDto,
    incomeFlag = true,
    date = getUnixTime(2021, 2, 15),
    categoryDto = CategoryDto(id = 1, name = "Car", iconName = "cars", iconColor = "#13A4CB", defaultFlag = true)
)

val mockWalletDto = WalletDto(
    id = 1,
    name = "koshel",
    balance = mockBalanceDto,
    income = mockBalanceDto,
    expense = mockBalanceDto,
)

fun putWithAuth(uri: String, body: Any, userId: Long): MockHttpServletRequestBuilder {
    val json: String = ObjectMapper().writeValueAsString(body)
    return MockMvcRequestBuilders.put(uri)
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .content(json)
        .header("userId", userId)
}

fun invokeByIdWithAuth(
    objectAddress: String,
    indexOfObject: Long,
    userId: Long,
    requestMethod: (String) -> MockHttpServletRequestBuilder
): MockHttpServletRequestBuilder {
    return requestMethod.invoke("/$objectAddress/$indexOfObject").with(authentication(userId))
        .accept(MediaType.APPLICATION_JSON)
}

fun getAllWithAuth(objectAddress: String, userId: Long): MockHttpServletRequestBuilder {
    return MockMvcRequestBuilders.get("/$objectAddress").with(authentication(userId)).accept(MediaType.APPLICATION_JSON)
}

fun authentication(userId: Long): RequestPostProcessor {
    return RequestPostProcessor { request: MockHttpServletRequest ->
        request.addHeader("userId", userId)
        request
    }
}

fun getResultMatcherWalletDtoWithJson(prefix: String, walletDto: WalletDto): ResultMatcher {
    return ResultMatcher {
        MockMvcResultMatchers.jsonPath("$prefix.id").value(walletDto.id)
        MockMvcResultMatchers.jsonPath("$prefix.name").value(walletDto.name)
        MockMvcResultMatchers.jsonPath("$prefix.balance.currencyDto.id").value(walletDto.balance.currencyDto.id)
        MockMvcResultMatchers.jsonPath("$prefix.balance.currencyDto.shortName")
            .value(walletDto.balance.currencyDto.shortName)
        MockMvcResultMatchers.jsonPath("$prefix.balance.currencyDto.longName")
            .value(walletDto.balance.currencyDto.longName)
        MockMvcResultMatchers.jsonPath("$prefix.balance.amount").value(walletDto.balance.amount)
    }
}

fun getResultMatcherTransactionDtoWithJson(prefix: String, transactionDto: TransactionDto): ResultMatcher {
    return ResultMatcher {
        MockMvcResultMatchers.jsonPath("$prefix.id").value(transactionDto.id)
        MockMvcResultMatchers.jsonPath("$prefix.balanceDto.currencyDto.id")
            .value(transactionDto.balanceDto.currencyDto.id)
        MockMvcResultMatchers.jsonPath("$prefix.balanceDto.currencyDto.shortName")
            .value(transactionDto.balanceDto.currencyDto.shortName)
        MockMvcResultMatchers.jsonPath("$prefix.balanceDto.currencyDto.longName")
            .value(transactionDto.balanceDto.currencyDto.longName)
        MockMvcResultMatchers.jsonPath("$prefix.balanceDto.amount").value(transactionDto.balanceDto.amount)
        MockMvcResultMatchers.jsonPath("$prefix.date").value(transactionDto.date)
        MockMvcResultMatchers.jsonPath("$prefix.categoryDto.id").value(transactionDto.categoryDto.id)
        MockMvcResultMatchers.jsonPath("$prefix.categoryDto.name").value(transactionDto.categoryDto.name)
        MockMvcResultMatchers.jsonPath("$prefix.categoryDto.iconColor").value(transactionDto.categoryDto.iconColor)
    }
}

fun getResultMatcherUserDtoWithJson(prefix: String, userDto: UserDto): ResultMatcher {
    return ResultMatcher {
        MockMvcResultMatchers.jsonPath("$prefix.id").value(userDto.id)
        MockMvcResultMatchers.jsonPath("$prefix.mail").value(userDto.mail)
    }
}
