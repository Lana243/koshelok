@file:Suppress("NonAsciiCharacters")

package sirius.tinkoff.koshelok

import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import sirius.tinkoff.koshelok.component.TransactionComponent
import sirius.tinkoff.koshelok.component.UserComponent
import sirius.tinkoff.koshelok.component.WalletComponent
import sirius.tinkoff.koshelok.dto.*
import sirius.tinkoff.koshelok.exceptions.TransactionIllegalAccessException
import sirius.tinkoff.koshelok.exceptions.UserNotFoundException
import sirius.tinkoff.koshelok.exceptions.WalletIllegalAccessException

@AutoConfigureMockMvc
@SpringBootTest
class WebLayerTest(@Autowired val mockMvc: MockMvc) {
    @MockkBean
    private lateinit var userComponent: UserComponent

    @MockkBean
    private lateinit var walletComponent: WalletComponent

    @MockkBean
    private lateinit var transactionComponent: TransactionComponent

    @Test
    fun `Юзеры успешно распарсились в json и вернулось 200`() {
        val listOfUsersDto = listOf(UserDto(1, "email1.com"), UserDto(2, "email2.com"))
        every { userComponent.getAll() } returns listOfUsersDto

        mockMvc.perform(get("/user").accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(getResultMatcherUserDtoWithJson("$.[0]", listOfUsersDto[0]))
            .andExpect(getResultMatcherUserDtoWithJson("$.[1]", listOfUsersDto[1]))
    }

    @Test
    fun `При выкидывании UserNotFoundException, возвращается 404`() {
        every { userComponent.get(3) } throws UserNotFoundException(3)

        mockMvc.perform(get("/user/3").accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound)
    }

    @Test
    fun `Кошельки успешно распарсились в json и вернулось 200`() {
        every { walletComponent.getAll(1) } returns listOf(
            mockWalletDto
        )

        mockMvc.perform(getAllWithAuth("wallet", 1))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(getResultMatcherWalletDtoWithJson("$.[0]", mockWalletDto))
    }

    @Test
    fun `Определенный кошелёк юзера успешно распарсился в json и вернулось 200`() {
        every { walletComponent.get(1, 1) } returns mockWalletDto

        mockMvc.perform(invokeByIdWithAuth("wallet", 1, 1) { get(it) })
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(getResultMatcherWalletDtoWithJson("$", mockWalletDto))
    }

    @Test
    fun `Успешное удаление кошелька возвращает 200`() {
        every { walletComponent.delete(1, 1) } returns Unit

        mockMvc.perform(invokeByIdWithAuth("wallet", 1, 1) { delete(it) })
            .andExpect(status().isOk)
    }

    @Test
    fun `При выкидывании WalletIllegalAccessException, возвращается 403`() {
        every { walletComponent.delete(1, 1) } throws WalletIllegalAccessException(1, 1)

        mockMvc.perform(invokeByIdWithAuth("wallet", 1, 1) { delete(it) })
            .andExpect(status().isForbidden)
    }

    @Test
    fun `Транзакции успешно распарсились в json и вернулось 200`() {
        every { transactionComponent.getAllByWalletId(1, 1) } returns listOf(
            mockTransactionDto
        )

        mockMvc.perform(getAllWithAuth("wallet/1/transaction", 1))
            .andExpect(status().isOk)
            .andExpect(getResultMatcherTransactionDtoWithJson("$.[0]", mockTransactionDto))
    }

    @Test
    fun `При выкидывании TransactionIllegalAccessException из transactionComponent, возвращается 403`() {
        every { transactionComponent.getAllByWalletId(1, 1) } throws TransactionIllegalAccessException(1, 1)

        mockMvc.perform(getAllWithAuth("wallet/1/transaction", 1))
            .andExpect(status().isForbidden)
    }

    @Test
    fun `Успешное удаление транзакции возвращает 200`() {
        every { transactionComponent.delete(1, 1, 1) } returns Unit

        mockMvc.perform(invokeByIdWithAuth("wallet/1/transaction", 1, 1) { delete(it) })
            .andExpect(status().isOk)
    }

    @Test
    fun `Успешное обновление транзакции возвращает 200`() {
        every { transactionComponent.update(1, 1, mockTransactionDto) } returns mockTransactionDto

        mockMvc.perform(putWithAuth("/wallet/1/transaction", mockTransactionDto, 1)).andExpect(status().isOk)
    }
}
