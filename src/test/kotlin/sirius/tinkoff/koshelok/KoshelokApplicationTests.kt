package sirius.tinkoff.koshelok

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import sirius.tinkoff.koshelok.controller.*
import sirius.tinkoff.koshelok.dto.*
import sirius.tinkoff.koshelok.exceptions.IllegalAmountException
import sirius.tinkoff.koshelok.exceptions.UserNotFoundException
import sirius.tinkoff.koshelok.exceptions.WalletNotFoundException
import kotlin.random.Random

fun getUnixTime(year: Long, month: Long, day: Long) = (year - 1970) * 31556926 + month * 2629743 + day * 86400

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class KoshelokApplicationTests(
    @Autowired private val userController: UserController,
    @Autowired private val walletController: WalletController,
    @Autowired private val transactionController: TransactionController,
    @Autowired private val currencyController: CurrencyController,
    @Autowired private val categoryController: CategoryController
) {
    private lateinit var actualListOfCategory: List<CategoryDto>
    private lateinit var actualListOfCurrency: List<CurrencyDto>

    @BeforeEach
    fun getCategoryAndCurrencyLists() {
        actualListOfCategory = categoryController.getDefault()
        actualListOfCurrency = currencyController.getAll()

        Assertions.assertTrue(actualListOfCategory.isNotEmpty())
        Assertions.assertTrue(actualListOfCurrency.isNotEmpty())
    }

    @Suppress("LongMethod")
    @Test
    fun happyPath() {
        // Создание юзера
        val actualUserDto = userController.save(UserDtoCreate("email@mail.com"))
        Assertions.assertEquals("email@mail.com", actualUserDto.mail)
        Assertions.assertEquals(1, userController.getAll().size)
        Assertions.assertEquals("email@mail.com", userController.get(actualUserDto.id).mail)

        val actualUserDtoForDelete = userController.save(UserDtoCreate("google@mail.com"))
        userController.delete(actualUserDtoForDelete.id)
        Assertions.assertThrows(UserNotFoundException::class.java) { userController.get(actualUserDtoForDelete.id) }

        Assertions.assertEquals(actualListOfCategory.size, categoryController.getAll(actualUserDto.id).size)

        // Проверка, что не создается пользователь с таким же email
        Assertions.assertEquals(actualUserDto, userController.save(UserDtoCreate("email@mail.com")))

        Assertions.assertTrue(walletController.getAll(actualUserDto.id).isEmpty())

        // Создание кошелька
        val actualCurrencyDto = CurrencyDto(1, "RUB", "Российский рубль")
        val actualWalletDto = walletController.save(
            actualUserDto.id,
            WalletDtoCreate("Eduardik", actualCurrencyDto)
        )
        Assertions.assertEquals("Eduardik", actualWalletDto.name)
        Assertions.assertEquals(BalanceDto(actualCurrencyDto, "0"), actualWalletDto.balance)
        Assertions.assertEquals(1, walletController.getAll(actualUserDto.id).size)

        val actualWalletDtoForDelete = walletController.save(
            actualUserDto.id,
            WalletDtoCreate("Eduardik", actualCurrencyDto)
        )
        walletController.delete(actualUserDto.id, actualWalletDtoForDelete.id)
        Assertions.assertThrows(WalletNotFoundException::class.java) {
            walletController.get(actualUserDto.id, actualWalletDtoForDelete.id)
        }

        // Получение транзакций пустого кошелька
        val actualListOfWallets = transactionController.getAll(actualUserDto.id, actualWalletDto.id)
        Assertions.assertTrue(actualListOfWallets.isEmpty())

        val incomesNum = 15
        // Создание транзакций и получение всех транзакций по id раннее созданного кошелька
        for (i in 1..incomesNum) {
            transactionController.save(
                actualUserDto.id,
                actualWalletDto.id,
                TransactionDtoCreate(
                    BalanceDto(actualListOfCurrency[0], "$i"),
                    true,
                    getUnixTime(Random.nextLong(50), Random.nextLong(12), Random.nextLong(30)),
                    actualListOfCategory[2]
                )
            )
        }

        val actualCategoryDto =
            categoryController.save(actualUserDto.id, CategoryDtoCreate("Psina", "psina", "#542135"))
        Assertions.assertEquals("Psina", actualCategoryDto.name)

        val updatedCategoryDto = categoryController.update(actualUserDto.id, actualCategoryDto.copy(name = "Koshka"))
        Assertions.assertEquals("Koshka", updatedCategoryDto.name)

        var actualWallet = walletController.get(actualUserDto.id, actualWalletDto.id)
        Assertions.assertEquals((1L..incomesNum).sum(), actualWallet.income.amountToLong())
        Assertions.assertEquals(0L, actualWallet.expense.amountToLong())
        Assertions.assertEquals((1L..incomesNum).sum(), actualWallet.balance.amountToLong())
        Assertions.assertEquals(
            transactionController.getAll(actualUserDto.id, actualWalletDto.id).count { it.incomeFlag }, incomesNum
        )
        Assertions.assertEquals(transactionController.getAll(actualUserDto.id, actualWalletDto.id).size, incomesNum)

        val actualBatch = transactionController.getBatch(actualUserDto.id, actualWalletDto.id, 5, 10)
        Assertions.assertEquals(0, actualBatch.size)

        val expenseNum = 6
        for (i in 1..expenseNum) {
            transactionController.save(
                actualUserDto.id,
                actualWalletDto.id,
                TransactionDtoCreate(
                    BalanceDto(actualListOfCurrency[0], "$i"),
                    false,
                    getUnixTime(Random.nextLong(50), Random.nextLong(12), Random.nextLong(30)),
                    actualListOfCategory[1]
                )
            )
        }

        actualWallet = walletController.get(actualUserDto.id, actualWalletDto.id)
        Assertions.assertEquals((1L..incomesNum).sum(), actualWallet.income.amountToLong())
        Assertions.assertEquals((1L..expenseNum).sum(), actualWallet.expense.amountToLong())
        Assertions.assertEquals((1L..incomesNum).sum() - (1L..expenseNum).sum(), actualWallet.balance.amountToLong())
        Assertions.assertEquals(
            expenseNum,
            transactionController.getAll(actualUserDto.id, actualWalletDto.id).count { !it.incomeFlag }
        )
        Assertions.assertEquals(
            incomesNum + expenseNum,
            transactionController.getAll(actualUserDto.id, actualWalletDto.id).size
        )

        Assertions.assertThrows(IllegalAmountException::class.java) {
            transactionController.save(
                actualUserDto.id,
                actualWalletDto.id,
                TransactionDtoCreate(
                    BalanceDto(actualListOfCurrency[0], "wqetqwfv"),
                    false,
                    getUnixTime(Random.nextLong(50), Random.nextLong(12), Random.nextLong(30)),
                    actualListOfCategory[1]
                )
            )
        }

        val createdTransactionDto = transactionController.save(
            actualUserDto.id,
            actualWalletDto.id,
            TransactionDtoCreate(
                BalanceDto(actualListOfCurrency[0], "150"),
                false,
                getUnixTime(Random.nextLong(50), Random.nextLong(12), Random.nextLong(30)),
                actualListOfCategory[1]
            )
        )
        val updatedTransactionDto = transactionController.update(
            actualUserDto.id,
            actualWalletDto.id,
            createdTransactionDto.copy(incomeFlag = true)
        )
        Assertions.assertEquals(true, updatedTransactionDto.incomeFlag)

        // Удаление транзакций
        val deletedTransaction = transactionController.getAll(actualUserDto.id, actualWalletDto.id)[0]
        transactionController.delete(actualUserDto.id, actualWalletDto.id, updatedTransactionDto.id)
        transactionController.delete(actualUserDto.id, actualWalletDto.id, deletedTransaction.id)
        Assertions.assertEquals(
            incomesNum + expenseNum - 1,
            transactionController.getAll(actualUserDto.id, actualWalletDto.id).size
        )
        Assertions.assertTrue(
            transactionController.getAll(actualUserDto.id, actualWalletDto.id).none { it.id == deletedTransaction.id }
        )
        actualWallet = walletController.get(actualUserDto.id, actualWalletDto.id)
        val realIncome =
            (1L..incomesNum).sum() -
                if (deletedTransaction.incomeFlag) deletedTransaction.balanceDto.amountToLong() else 0
        val realExpense =
            (1L..expenseNum).sum() -
                if (!deletedTransaction.incomeFlag) deletedTransaction.balanceDto.amountToLong() else 0

        Assertions.assertEquals(realIncome, actualWallet.income.amountToLong())
        Assertions.assertEquals(realExpense, actualWallet.expense.amountToLong())
        Assertions.assertEquals(realIncome - realExpense, actualWallet.balance.amountToLong())

        Assertions.assertTrue(walletController.getAll(actualUserDto.id).isNotEmpty())

        val actualTransactions = transactionController.getAll(actualUserDto.id, actualWalletDto.id)
        for (i in 1 until incomesNum + expenseNum - 1) {
            Assertions.assertTrue(actualTransactions[i - 1].date > actualTransactions[i].date)
        }
    }

    @Test
    fun contextLoads() {
    }
}
