package sirius.tinkoff.koshelok

import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.repository.findByIdOrNull
import sirius.tinkoff.koshelok.entity.CategoryEntity
import sirius.tinkoff.koshelok.entity.CurrencyEntity
import sirius.tinkoff.koshelok.entity.TransactionEntity
import sirius.tinkoff.koshelok.entity.UserEntity
import sirius.tinkoff.koshelok.entity.WalletEntity
import sirius.tinkoff.koshelok.exceptions.*

interface UserRepository : CrudRepository<UserEntity, Long> {
    fun findByMail(userMail: String): UserEntity?
}

fun UserRepository.findByUserId(userId: Long): UserEntity =
    findByIdOrNull(userId) ?: throw UserNotFoundException(userId)

interface WalletRepository : CrudRepository<WalletEntity, Long> {
    @Query(
        "SELECT new kotlin.Pair(incomeFlag, SUM(value)) " +
            "FROM transactions WHERE walletId = :walletId GROUP BY incomeFlag"
    )
    fun getIncomeAndExpenseByWalletId(walletId: Long): List<Pair<Boolean, Long>>

    fun findAllByUserId(userId: Long): List<WalletEntity>
}

fun WalletRepository.findByWalletId(walletId: Long): WalletEntity =
    findByIdOrNull(walletId) ?: throw WalletNotFoundException(walletId)

interface TransactionRepository : PagingAndSortingRepository<TransactionEntity, Long> {
    fun findAllByWalletIdOrderByTimestampDesc(walletId: Long): List<TransactionEntity>
    fun findAllByWalletIdOrderByTimestampDesc(walletId: Long, pageable: Pageable): List<TransactionEntity>
}

fun TransactionRepository.findByTransactionId(transactionId: Long) =
    findByIdOrNull(transactionId) ?: throw TransactionNotFoundException(transactionId)

interface CategoryRepository : CrudRepository<CategoryEntity, Long> {
    fun findAllByUserIdIsNull(): List<CategoryEntity>
    fun findAllByUserIdOrUserIdIsNull(userId: Long): List<CategoryEntity>
}

fun CategoryRepository.findByCategoryId(categoryId: Long) =
    findByIdOrNull(categoryId) ?: throw CategoryNotFoundException(categoryId)

interface CurrencyRepository : CrudRepository<CurrencyEntity, Long>

fun CurrencyRepository.findByCurrencyId(currencyId: Long): CurrencyEntity =
    findByIdOrNull(currencyId) ?: throw CurrencyNotFoundException(currencyId)
