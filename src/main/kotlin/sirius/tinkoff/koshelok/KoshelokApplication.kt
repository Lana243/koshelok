package sirius.tinkoff.koshelok

import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import sirius.tinkoff.koshelok.entity.CategoryEntity
import sirius.tinkoff.koshelok.entity.CurrencyEntity

@SpringBootApplication
class KoshelokApplication {
    @Bean
    fun initCurrency(currencyRepository: CurrencyRepository) = CommandLineRunner {
        currencyRepository.save(CurrencyEntity(id = 1, shortName = "RUB", longName = "Российский рубль"))
        currencyRepository.save(CurrencyEntity(id = 2, shortName = "USD", longName = "Доллар США"))
        currencyRepository.save(CurrencyEntity(id = 3, shortName = "GBP", longName = "Фунт стерлингов"))
        currencyRepository.save(CurrencyEntity(id = 4, shortName = "EUR", longName = "Евро"))
    }

    @Bean
    fun initCategory(categoryRepository: CategoryRepository) = CommandLineRunner {
        categoryRepository.save(
            CategoryEntity(
                id = 1,
                name = "Кафе и рестораны",
                iconName = "fork.knife",
                iconColor = "#7765C0"
            )
        )
        categoryRepository.save(
            CategoryEntity(
                id = 2,
                name = "Супермаркеты",
                iconName = "cart",
                iconColor = "#339FEE"
            )
        )
        categoryRepository.save(
            CategoryEntity(
                id = 3,
                name = "Спортзал",
                iconName = "figure.walk",
                iconColor = "#994747"
            )
        )
        categoryRepository.save(
            CategoryEntity(
                id = 4,
                name = "Общественный транспорт",
                iconName = "bus",
                iconColor = "#EE33BA"
            )
        )
        categoryRepository.save(
            CategoryEntity(
                id = 5,
                name = "Медицина",
                iconName = "pills.fill",
                iconColor = "#16DC71"
            )
        )
        categoryRepository.save(
            CategoryEntity(
                id = 6,
                name = "Бензин",
                iconName = "fuelpump.fill",
                iconColor = "#EEA333"
            )
        )
        categoryRepository.save(
            CategoryEntity(
                id = 7,
                name = "Квартплата",
                iconName = "house.fill",
                iconColor = "#91397D"
            )
        )
        categoryRepository.save(
            CategoryEntity(
                id = 8,
                name = "Отпуск",
                iconName = "sun.max.fill",
                iconColor = "#EEDB33"
            )
        )
        categoryRepository.save(
            CategoryEntity(
                id = 9,
                name = "Зарплата",
                iconName = "banknote",
                iconColor = "#438A29"
            )
        )
        categoryRepository.save(
            CategoryEntity(
                id = 10,
                name = "Прочее",
                iconName = "ellipsis",
                iconColor = "#969AB4"
            )
        )
    }
}

fun main(args: Array<String>) {
    runApplication<KoshelokApplication>(*args)
}
