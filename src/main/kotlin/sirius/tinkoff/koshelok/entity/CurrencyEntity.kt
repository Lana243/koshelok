package sirius.tinkoff.koshelok.entity

import javax.persistence.*

@Entity(name = "currencies")
@SequenceGenerator(allocationSize = 1, name = "currency_seq", sequenceName = "currency_seq")
data class CurrencyEntity(
    val shortName: String,
    val longName: String,
    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "currency_seq") val id: Long? = null
)
