package sirius.tinkoff.koshelok.entity

import javax.persistence.*

@Entity(name = "users")
@SequenceGenerator(allocationSize = 1, name = "user_seq", sequenceName = "user_seq")
data class UserEntity(
    val mail: String,
    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_seq") val id: Long? = null
)
