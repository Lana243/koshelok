package sirius.tinkoff.koshelok.entity

import javax.persistence.*

@Entity(name = "transactions")
@SequenceGenerator(allocationSize = 1, name = "transaction_seq", sequenceName = "transaction_seq")
data class TransactionEntity(
    val value: Long,
    val incomeFlag: Boolean,
    val timestamp: Long,
    val walletId: Long,
    val categoryId: Long,
    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transaction_seq") val id: Long? = null
)
