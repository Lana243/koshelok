package sirius.tinkoff.koshelok.entity

import javax.persistence.*

@Entity(name = "categories")
@SequenceGenerator(allocationSize = 1, name = "category_seq", sequenceName = "category_seq")
data class CategoryEntity(
    val name: String,
    val iconName: String,
    val iconColor: String,
    val userId: Long? = null, // если userId == null, то эта категория есть по умолчанию у всех пользователей
    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "category_seq") val id: Long? = null
)
