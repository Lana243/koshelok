package sirius.tinkoff.koshelok.entity

import javax.persistence.*

@Entity(name = "wallets")
@SequenceGenerator(allocationSize = 1, name = "wallet_seq", sequenceName = "wallet_seq")
data class WalletEntity(
    val name: String,
    val userId: Long,
    val currencyId: Long,
    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wallet_seq") val id: Long? = null
)
