package sirius.tinkoff.koshelok.controller

import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import sirius.tinkoff.koshelok.component.CurrencyComponent
import sirius.tinkoff.koshelok.dto.CurrencyDto

@RestController
@RequestMapping("/currency")
class CurrencyController(@Autowired private val currencyComponent: CurrencyComponent) {
    private val logger = KotlinLogging.logger {}

    @GetMapping
    fun getAll(): List<CurrencyDto> = logger.info { "GET /currency" }.let { currencyComponent.getAll() }
}
