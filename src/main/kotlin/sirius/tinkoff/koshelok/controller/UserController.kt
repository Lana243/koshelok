package sirius.tinkoff.koshelok.controller

import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import sirius.tinkoff.koshelok.component.UserComponent
import sirius.tinkoff.koshelok.dto.UserDto
import sirius.tinkoff.koshelok.dto.UserDtoCreate
import javax.validation.Valid

@RestController
@RequestMapping("/user")
class UserController(@Autowired private val userComponent: UserComponent) {
    private val logger = KotlinLogging.logger {}

    @GetMapping
    fun getAll(): List<UserDto> = logger.info { "GET /user" }.let { userComponent.getAll() }

    @GetMapping("/{userId}")
    fun get(@PathVariable userId: Long): UserDto = logger.info { "GET /user/$userId" }.let { userComponent.get(userId) }

    @DeleteMapping("/{userId}")
    fun delete(@PathVariable userId: Long): Unit =
        logger.info { "DELETE /user/$userId" }.let { userComponent.delete(userId) }

    @PostMapping
    fun save(@Valid @RequestBody userDtoCreate: UserDtoCreate): UserDto =
        logger.info { "POST /user/$userDtoCreate" }.let { userComponent.save(userDtoCreate) }
}
