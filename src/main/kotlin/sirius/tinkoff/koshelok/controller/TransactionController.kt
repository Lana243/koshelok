package sirius.tinkoff.koshelok.controller

import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import sirius.tinkoff.koshelok.component.TransactionComponent
import sirius.tinkoff.koshelok.dto.TransactionDto
import sirius.tinkoff.koshelok.dto.TransactionDtoCreate
import javax.validation.Valid

@RestController
@RequestMapping("wallet/{walletId}/transaction")
class TransactionController(@Autowired private val transactionComponent: TransactionComponent) {
    private val logger = KotlinLogging.logger {}

    @GetMapping
    fun getAll(@RequestHeader userId: Long, @PathVariable walletId: Long): List<TransactionDto> =
        logger.info { "GET wallet/$walletId/transaction with header userId=$userId" }
            .let { transactionComponent.getAllByWalletId(userId, walletId) }

    @GetMapping("/batch")
    fun getBatch(
        @RequestHeader userId: Long,
        @PathVariable walletId: Long,
        @RequestParam(defaultValue = "0") page: Int,
        @RequestParam(defaultValue = "5") size: Int
    ): List<TransactionDto> = logger.info { "GET wallet/$walletId/transaction/batch with header userId=$userId" }
        .let { transactionComponent.getBatch(userId, walletId, page, size) }

    @DeleteMapping("/{transactionId}")
    fun delete(@RequestHeader userId: Long, @PathVariable walletId: Long, @PathVariable transactionId: Long): Unit =
        logger.info { "DELETE wallet/$walletId/transaction/$transactionId with header userId=$userId" }
            .let { transactionComponent.delete(userId, walletId, transactionId) }

    @PostMapping
    fun save(
        @RequestHeader userId: Long,
        @PathVariable walletId: Long,
        @Valid @RequestBody transactionDtoCreate: TransactionDtoCreate
    ): TransactionDto =
        logger.info { "POST wallet/$walletId/transaction/$transactionDtoCreate with header userId=$userId" }
            .let { transactionComponent.save(userId, walletId, transactionDtoCreate) }

    @PutMapping
    fun update(
        @RequestHeader userId: Long,
        @PathVariable walletId: Long,
        @Valid @RequestBody transactionDto: TransactionDto
    ): TransactionDto =
        logger.info { "PUT wallet/$walletId/transaction/$transactionDto with header userId=$userId" }
            .let { transactionComponent.update(userId, walletId, transactionDto) }
}
