package sirius.tinkoff.koshelok.controller

import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import sirius.tinkoff.koshelok.component.CategoryComponent
import sirius.tinkoff.koshelok.dto.CategoryDto
import sirius.tinkoff.koshelok.dto.CategoryDtoCreate
import javax.validation.Valid

@RestController
@RequestMapping("/category")
class CategoryController(@Autowired private val categoryComponent: CategoryComponent) {
    private val logger = KotlinLogging.logger {}

    @GetMapping("/default")
    fun getDefault(): List<CategoryDto> = logger.info { "GET /category/default" }.let { categoryComponent.getDefault() }

    @GetMapping
    fun getAll(@RequestHeader userId: Long): List<CategoryDto> =
        logger.info { "GET /category with header userId=$userId" }.let { categoryComponent.getAll(userId) }

    @GetMapping("/{categoryId}")
    fun get(@RequestHeader userId: Long, @PathVariable categoryId: Long) =
        logger.info { "GET /category/$categoryId with header userId=$userId" }
            .let { categoryComponent.get(userId, categoryId) }

    @PostMapping
    fun save(@RequestHeader userId: Long, @Valid @RequestBody categoryDtoCreate: CategoryDtoCreate): CategoryDto =
        logger.info { "POST /category/$categoryDtoCreate with header userId=$userId" }
            .let { categoryComponent.save(userId, categoryDtoCreate) }

    @PutMapping
    fun update(@RequestHeader userId: Long, @Valid @RequestBody categoryDto: CategoryDto): CategoryDto =
        logger.info { "PUT /category/$categoryDto with header userId=$userId" }
            .let { categoryComponent.update(userId, categoryDto) }
}
