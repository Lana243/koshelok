package sirius.tinkoff.koshelok.controller

import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import sirius.tinkoff.koshelok.component.WalletComponent
import sirius.tinkoff.koshelok.dto.WalletDto
import sirius.tinkoff.koshelok.dto.WalletDtoCreate
import javax.validation.Valid

@RestController
@RequestMapping("/wallet")
class WalletController(@Autowired private val walletComponent: WalletComponent) {
    private val logger = KotlinLogging.logger {}

    @GetMapping
    fun getAll(@RequestHeader userId: Long): List<WalletDto> =
        logger.info { "GET /wallet with header userId=$userId" }.let { walletComponent.getAll(userId) }

    @GetMapping("/{walletId}")
    fun get(@RequestHeader userId: Long, @PathVariable walletId: Long): WalletDto =
        logger.info { "GET /wallet/$walletId with header userId=$userId" }.let { walletComponent.get(userId, walletId) }

    @DeleteMapping("/{walletId}")
    fun delete(@RequestHeader userId: Long, @PathVariable walletId: Long): Unit =
        logger.info { "DELETE /wallet/$walletId with header userId=$userId" }
            .let { walletComponent.delete(userId, walletId) }

    @PostMapping
    fun save(@RequestHeader userId: Long, @Valid @RequestBody walletDtoCreate: WalletDtoCreate): WalletDto =
        logger.info { "POST /wallet/$walletDtoCreate with header userId=$userId" }
            .let { walletComponent.save(userId, walletDtoCreate) }
}
