package sirius.tinkoff.koshelok.component

import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component
import sirius.tinkoff.koshelok.UserRepository
import sirius.tinkoff.koshelok.converter.toDto
import sirius.tinkoff.koshelok.converter.toEntity
import sirius.tinkoff.koshelok.dto.UserDto
import sirius.tinkoff.koshelok.dto.UserDtoCreate
import sirius.tinkoff.koshelok.exceptions.UserNotFoundException

@Component
class UserComponent(@Autowired private val userRepository: UserRepository) {
    private val logger = KotlinLogging.logger {}

    fun getAll(): List<UserDto> = userRepository.findAll().map { it.toDto() }
        .also { logger.info { "All(number=${it.size}) users are returned" } }

    fun get(userId: Long): UserDto =
        userRepository.findByIdOrNull(userId)?.toDto()
            ?.also { logger.info { "User(${it.id}) is returned" } }
            ?: throw UserNotFoundException(userId).also { logger.info { "User($userId) not found" } }

    fun delete(userId: Long): Unit =
        userRepository.deleteById(userId).also { logger.info { "User($userId) deleted" } }

    fun save(userDtoCreate: UserDtoCreate): UserDto = (
        userRepository.findByMail(userDtoCreate.mail)
            ?.also { logger.info { "User(${it.id}, ${it.mail}) authorized" } }
            ?: userRepository.save(userDtoCreate.toEntity())
                .also { logger.info { "User(${it.id}, ${it.mail}) created" } }
        ).toDto()
}
