package sirius.tinkoff.koshelok.component

import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Component
import sirius.tinkoff.koshelok.*
import sirius.tinkoff.koshelok.converter.toDto
import sirius.tinkoff.koshelok.converter.toEntity
import sirius.tinkoff.koshelok.dto.BalanceDto
import sirius.tinkoff.koshelok.dto.TransactionDto
import sirius.tinkoff.koshelok.dto.TransactionDtoCreate
import sirius.tinkoff.koshelok.entity.TransactionEntity

@Component
class TransactionComponent(
    @Autowired private val transactionRepository: TransactionRepository,
    @Autowired private val categoryRepository: CategoryRepository,
    @Autowired private val walletCurrencyComponent: WalletCurrencyComponent,
    @Autowired private val validationComponent: ValidationComponent
) {
    private val logger = KotlinLogging.logger {}

    fun getAllByWalletId(userId: Long, walletId: Long): List<TransactionDto> {
        validationComponent.validateWallet(userId, walletId)
        return transactionRepository.findAllByWalletIdOrderByTimestampDesc(walletId).map { it.toDto() }
            .also { logger.info { "User($userId) | Wallet($walletId) | All(${it.size}) transaction returned" } }
    }

    fun getBatch(userId: Long, walletId: Long, page: Int, size: Int): List<TransactionDto> {
        validationComponent.validateWallet(userId, walletId)
        return transactionRepository.findAllByWalletIdOrderByTimestampDesc(walletId, PageRequest.of(page, size))
            .map { it.toDto() }
            .also {
                logger.info {
                    "User($userId) | Wallet($walletId) | Page(number= $page, size=$size) with transactions is returned"
                }
            }
    }

    fun delete(userId: Long, walletId: Long, transactionId: Long) {
        validationComponent.validateTransaction(userId, walletId, transactionId)
        transactionRepository.deleteById(transactionId)
            .also { logger.info { "User($userId) | Wallet($walletId) | Transaction($transactionId) is deleted" } }
    }

    fun save(userId: Long, walletId: Long, transactionDtoCreate: TransactionDtoCreate): TransactionDto {
        validationComponent.validateWallet(userId, walletId)
        return transactionRepository.save(transactionDtoCreate.toEntity(walletId)).toDto()
            .also { logger.info { "User($userId) | Wallet($walletId) | Transaction(${it.id}) is created" } }
    }

    fun update(userId: Long, walletId: Long, transactionDto: TransactionDto): TransactionDto {
        validationComponent.validateWallet(userId, walletId)
        return transactionRepository.save(transactionDto.toEntity(walletId)).toDto()
            .also { logger.info { "User($userId) | Wallet($walletId) | Transaction(${it.id}) is updated" } }
    }

    fun TransactionEntity.toDto() = TransactionDto(
        id = id!!,
        balanceDto = BalanceDto(
            currencyDto = walletCurrencyComponent.currencyByWalletId(walletId).toDto(),
            amount = value.toString()
        ),
        incomeFlag = incomeFlag,
        date = timestamp,
        categoryDto = categoryRepository.findByCategoryId(categoryId).toDto()
    )
}
