package sirius.tinkoff.koshelok.component

import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import sirius.tinkoff.koshelok.WalletRepository
import sirius.tinkoff.koshelok.converter.toDto
import sirius.tinkoff.koshelok.converter.toEntity
import sirius.tinkoff.koshelok.dto.BalanceDto
import sirius.tinkoff.koshelok.dto.WalletDto
import sirius.tinkoff.koshelok.dto.WalletDtoCreate
import sirius.tinkoff.koshelok.entity.WalletEntity
import sirius.tinkoff.koshelok.findByWalletId

@Component
class WalletComponent(
    @Autowired private val walletRepository: WalletRepository,
    @Autowired private val repositoryManager: WalletCurrencyComponent,
    @Autowired private val validationComponent: ValidationComponent
) {
    private val logger = KotlinLogging.logger {}

    fun getAll(userId: Long) = walletRepository.findAllByUserId(userId).map { it.toDto() }
        .also { logger.info { "User($userId) | All(${it.size}) wallets returned" } }

    fun get(userId: Long, walletId: Long): WalletDto {
        validationComponent.validateWallet(userId, walletId)
        return walletRepository.findByWalletId(walletId).toDto()
            .also { logger.info { "User($userId) | Wallet($walletId) is returned" } }
    }

    fun delete(userId: Long, walletId: Long) {
        validationComponent.validateWallet(userId, walletId)
        walletRepository.deleteById(walletId).also { logger.info { "User($userId) | Wallet($walletId) is deleted" } }
    }

    fun save(userId: Long, walletDtoCreate: WalletDtoCreate): WalletDto =
        walletRepository.save(walletDtoCreate.toEntity(userId)).toDto()
            .also { logger.info { "User($userId) | Wallet(${it.id}) is created" } }

    fun WalletEntity.toDto(): WalletDto {
        val amountByIncomeFlag: Map<Boolean, Long> =
            walletRepository.getIncomeAndExpenseByWalletId(id!!).associate { it.first to it.second }
        val income = amountByIncomeFlag[true] ?: 0
        val expense = amountByIncomeFlag[false] ?: 0
        val currencyDto = repositoryManager.currencyByWalletId(id).toDto()
        return WalletDto(
            id = id,
            name = name,
            balance = BalanceDto(currencyDto = currencyDto, amount = (income - expense).toString()),
            income = BalanceDto(currencyDto = currencyDto, amount = income.toString()),
            expense = BalanceDto(currencyDto = currencyDto, amount = expense.toString()),
        )
    }
}
