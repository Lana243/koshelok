package sirius.tinkoff.koshelok.component

import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import sirius.tinkoff.koshelok.CurrencyRepository
import sirius.tinkoff.koshelok.converter.toDto
import sirius.tinkoff.koshelok.dto.CurrencyDto

@Component
class CurrencyComponent(@Autowired private val currencyRepository: CurrencyRepository) {
    private val logger = KotlinLogging.logger {}

    fun getAll(): List<CurrencyDto> = currencyRepository.findAll().map { it.toDto() }
        .also { logger.info { "All(size=${it.size}) currencies is returned" } }
}
