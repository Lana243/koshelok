package sirius.tinkoff.koshelok.component

import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import sirius.tinkoff.koshelok.CategoryRepository
import sirius.tinkoff.koshelok.converter.toDto
import sirius.tinkoff.koshelok.converter.toEntity
import sirius.tinkoff.koshelok.dto.CategoryDto
import sirius.tinkoff.koshelok.dto.CategoryDtoCreate
import sirius.tinkoff.koshelok.findByCategoryId

@Component
class CategoryComponent(@Autowired private val categoryRepository: CategoryRepository) {
    private val logger = KotlinLogging.logger {}

    fun getDefault(): List<CategoryDto> = categoryRepository.findAllByUserIdIsNull().map { it.toDto() }
        .also { logger.info { "All(size=${it.size}) default categories is returned" } }

    @Suppress("ForbiddenComment", "UnusedPrivateMember")
    // TODO: добавить проверку, что пользователь с таким userId есть у нас в БД
    fun getAll(userId: Long): List<CategoryDto> =
        categoryRepository.findAllByUserIdOrUserIdIsNull(userId).map { it.toDto() }
            .also { logger.info { "User($userId) | All(size=${it.size}) default and custom categories is returned" } }

    @Suppress("ForbiddenComment", "UnusedPrivateMember")
    // TODO: проверить, что у юзера есть categoryId
    fun get(userId: Long, categoryId: Long) = categoryRepository.findByCategoryId(categoryId).toDto()
        .also { logger.info { "User($userId) | Category(${it.id}) is returned" } }

    @Suppress("ForbiddenComment", "UnusedPrivateMember")
    // TODO: добавить проверку, что пользователь с таким userId есть у нас в БД
    fun save(userId: Long, categoryDtoCreate: CategoryDtoCreate): CategoryDto =
        categoryRepository.save(categoryDtoCreate.toEntity(userId)).toDto()
            .also { logger.info { "User($userId) | Category(${it.id}) is created" } }

    @Suppress("ForbiddenComment", "UnusedPrivateMember")
    // TODO: проверить, что userId == categoryDto.userId и что у user есть categoryDto.id
    fun update(userId: Long, categoryDto: CategoryDto): CategoryDto =
        categoryRepository.save(categoryDto.toEntity(userId)).toDto()
            .also { logger.info { "User($userId) | Category(${it.id}) is updated" } }
}
