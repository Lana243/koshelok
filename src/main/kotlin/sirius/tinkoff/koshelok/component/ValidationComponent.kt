package sirius.tinkoff.koshelok.component

import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import sirius.tinkoff.koshelok.TransactionRepository
import sirius.tinkoff.koshelok.WalletRepository
import sirius.tinkoff.koshelok.exceptions.TransactionIllegalAccessException
import sirius.tinkoff.koshelok.exceptions.WalletIllegalAccessException
import sirius.tinkoff.koshelok.findByTransactionId
import sirius.tinkoff.koshelok.findByWalletId

@Component
class ValidationComponent(
    @Autowired private val walletRepository: WalletRepository,
    @Autowired private val transactionRepository: TransactionRepository
) {
    private val logger = KotlinLogging.logger {}

    fun validateWallet(userId: Long, walletId: Long) {
        if (walletRepository.findByWalletId(walletId).userId != userId) {
            throw WalletIllegalAccessException(
                userId,
                walletId
            ).also { logger.error { "User($userId) | User doesn't have access to wallet($walletId)" } }
        }
    }

    fun validateTransaction(userId: Long, walletId: Long, transactionId: Long) {
        validateWallet(userId, walletId)
        if (transactionRepository.findByTransactionId(transactionId).walletId != walletId) {
            throw TransactionIllegalAccessException(
                walletId,
                transactionId
            ).also { logger.error { "User($userId) | Transaction doesn't belong to wallet($walletId)" } }
        }
    }
}
