package sirius.tinkoff.koshelok.component

import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import sirius.tinkoff.koshelok.*
import sirius.tinkoff.koshelok.entity.CurrencyEntity

@Component
class WalletCurrencyComponent(
    @Autowired val walletRepository: WalletRepository,
    @Autowired val currencyRepository: CurrencyRepository
) {
    private val logger = KotlinLogging.logger {}

    fun currencyByWalletId(walletId: Long): CurrencyEntity {
        val walletEntity = walletRepository.findByWalletId(walletId)
        return currencyRepository.findByCurrencyId(walletEntity.currencyId)
            .also { logger.info("Currency(${it.shortName}) belonging to wallet($walletId) returned") }
    }
}
