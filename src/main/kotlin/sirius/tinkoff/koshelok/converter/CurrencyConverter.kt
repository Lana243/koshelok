package sirius.tinkoff.koshelok.converter

import sirius.tinkoff.koshelok.dto.CurrencyDto
import sirius.tinkoff.koshelok.entity.CurrencyEntity

fun CurrencyDto.toEntity() = CurrencyEntity(
    id = id,
    shortName = shortName,
    longName = longName
)

fun CurrencyEntity.toDto() = CurrencyDto(
    id = id!!,
    shortName = shortName,
    longName = longName
)
