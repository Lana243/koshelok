package sirius.tinkoff.koshelok.converter

import sirius.tinkoff.koshelok.dto.WalletDtoCreate
import sirius.tinkoff.koshelok.entity.WalletEntity

fun WalletDtoCreate.toEntity(userId: Long) = WalletEntity(
    name = name,
    userId = userId,
    currencyId = currencyDto.id
)
