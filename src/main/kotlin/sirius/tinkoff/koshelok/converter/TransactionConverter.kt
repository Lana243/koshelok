package sirius.tinkoff.koshelok.converter

import sirius.tinkoff.koshelok.dto.*
import sirius.tinkoff.koshelok.entity.TransactionEntity

fun TransactionDtoCreate.toEntity(walletId: Long) = TransactionEntity(
    value = balanceDto.amountToLong(),
    incomeFlag = incomeFlag,
    timestamp = date,
    walletId = walletId,
    categoryId = categoryDto.id,
)

fun TransactionDto.toEntity(walletId: Long) = TransactionEntity(
    id = id,
    value = balanceDto.amountToLong(),
    incomeFlag = incomeFlag,
    timestamp = date,
    walletId = walletId,
    categoryId = categoryDto.id
)
