package sirius.tinkoff.koshelok.converter

import sirius.tinkoff.koshelok.dto.CategoryDto
import sirius.tinkoff.koshelok.dto.CategoryDtoCreate
import sirius.tinkoff.koshelok.entity.CategoryEntity

fun CategoryEntity.toDto() = CategoryDto(
    id = id!!,
    name = name,
    iconName = iconName,
    iconColor = iconColor,
    defaultFlag = userId == null
)

fun CategoryDtoCreate.toEntity(userId: Long) = CategoryEntity(
    name = name,
    iconName = iconName,
    iconColor = iconColor,
    userId = userId
)

fun CategoryDto.toEntity(userId: Long) = CategoryEntity(
    id = id,
    name = name,
    iconName = iconName,
    iconColor = iconColor,
    userId = userId
)
