package sirius.tinkoff.koshelok.dto

import io.swagger.v3.oas.annotations.media.Schema
import sirius.tinkoff.koshelok.exceptions.IllegalAmountException
import javax.validation.Valid
import javax.validation.constraints.Pattern

const val AMOUNT_REGEXP = "\\d{1,19}"

data class BalanceDto(
    @get:Valid
    val currencyDto: CurrencyDto,

    @field:Schema(example = "228")
    @field:Pattern(regexp = AMOUNT_REGEXP, message = "Invalid amount: expected adequate number")
    val amount: String
)

fun BalanceDto.amountToLong() = amount.toLongOrNull() ?: throw IllegalAmountException(amount)
