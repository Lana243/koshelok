package sirius.tinkoff.koshelok.dto

import io.swagger.v3.oas.annotations.media.Schema
import javax.validation.constraints.Pattern

const val VALID_MAIL_REGEXP = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}\$"

data class UserDtoCreate(
    @field:Schema(example = "anton@mail.com")
    @field:Pattern(regexp = VALID_MAIL_REGEXP, message = "Invalid mail")
    val mail: String
)

data class UserDto(
    @field:Schema(example = "1")
    val id: Long,

    @field:Schema(example = "anton@mail.com")
    @field:Pattern(regexp = VALID_MAIL_REGEXP, message = "Invalid mail")
    val mail: String
)
