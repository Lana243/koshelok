package sirius.tinkoff.koshelok.dto

import io.swagger.v3.oas.annotations.media.Schema
import javax.validation.constraints.NotBlank

data class CurrencyDto(
    @field:Schema(example = "1")
    val id: Long,

    @field:Schema(example = "RUB")
    @field:NotBlank(message = "shortName must be not blank")
    val shortName: String,

    @field:Schema(example = "Российский рубль")
    @field:NotBlank(message = "longName must be not blank")
    val longName: String
)
