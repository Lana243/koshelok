package sirius.tinkoff.koshelok.dto

import io.swagger.v3.oas.annotations.media.Schema
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Pattern

const val HEX_COLOR_REGEXP = "^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$"

data class CategoryDtoCreate(
    @field:Schema(example = "Клубы")
    @field:NotBlank(message = "name must be not blank")
    val name: String,

    @field:Schema(example = "pills.fill")
    @field:NotBlank(message = "iconName must be not blank")
    val iconName: String,

    @field:Schema(example = "#FF00FF")
    @field:Pattern(regexp = HEX_COLOR_REGEXP, message = "iconColor must be valid hex color")
    val iconColor: String,
)

data class CategoryDto(
    @field:Schema(example = "1")
    val id: Long,

    @field:Schema(example = "Кафе и рестораны")
    @field:NotBlank(message = "name must be not blank")
    val name: String,

    @field:Schema(example = "fork.knife")
    @field:NotBlank(message = "iconName must be not blank")
    val iconName: String,

    @field:Schema(example = "#7765C0")
    @field:Pattern(regexp = HEX_COLOR_REGEXP, message = "iconColor must be valid hex color")
    val iconColor: String,

    val defaultFlag: Boolean
)
