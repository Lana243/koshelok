package sirius.tinkoff.koshelok.dto

import io.swagger.v3.oas.annotations.media.Schema
import javax.validation.Valid
import javax.validation.constraints.NotBlank

data class WalletDtoCreate(
    @field:Schema(example = "wallet")
    @field:NotBlank(message = "blank name")
    val name: String,

    @get:Valid
    val currencyDto: CurrencyDto
)

data class WalletDto(
    @Schema(example = "1")
    val id: Long,

    @field:Schema(example = "wallet")
    @field:NotBlank(message = "blank name")
    val name: String,

    @get:Valid
    val balance: BalanceDto,

    @get:Valid
    val income: BalanceDto,

    @get:Valid
    val expense: BalanceDto
)
