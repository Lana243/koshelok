package sirius.tinkoff.koshelok.dto

import io.swagger.v3.oas.annotations.media.Schema
import javax.validation.Valid
import javax.validation.constraints.Min

data class TransactionDtoCreate(
    @get:Valid
    val balanceDto: BalanceDto,

    val incomeFlag: Boolean,

    @Min(0)
    val date: Long,

    @get:Valid
    val categoryDto: CategoryDto
)

data class TransactionDto(
    @Schema(example = "1")
    val id: Long,

    @get:Valid
    val balanceDto: BalanceDto,

    val incomeFlag: Boolean,

    @Min(0)
    val date: Long,

    @get:Valid
    val categoryDto: CategoryDto
)
