package sirius.tinkoff.koshelok.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import java.lang.IllegalArgumentException

@ResponseStatus(HttpStatus.BAD_REQUEST)
class IllegalAmountException(private val amount: String) : IllegalArgumentException() {
    override val message: String
        get() = "Expected amount to be an integer, but found $amount"
}
