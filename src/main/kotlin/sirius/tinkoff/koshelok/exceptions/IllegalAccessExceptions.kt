package sirius.tinkoff.koshelok.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.FORBIDDEN)
class WalletIllegalAccessException(private val userId: Long, private val walletId: Long) : IllegalAccessException() {
    override val message: String
        get() = "User $userId cannot access wallet $walletId"
}

@ResponseStatus(HttpStatus.FORBIDDEN)
class TransactionIllegalAccessException(private val walletId: Long, private val transactionId: Long) :
    IllegalAccessException() {
    override val message: String
        get() = "Wallet $walletId cannot access transaction $transactionId"
}
