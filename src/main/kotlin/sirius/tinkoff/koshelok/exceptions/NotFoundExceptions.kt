package sirius.tinkoff.koshelok.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
open class NotFoundException(private val instanceName: String, private val id: Long) : RuntimeException() {
    override val message: String
        get() = "There is no $instanceName with id=$id"
}

class UserNotFoundException(userId: Long) : NotFoundException("user", userId)

class WalletNotFoundException(walletId: Long) : NotFoundException("wallet", walletId)

class TransactionNotFoundException(transactionId: Long) : NotFoundException("transaction", transactionId)

class CategoryNotFoundException(categoryId: Long) : NotFoundException("category", categoryId)

class CurrencyNotFoundException(currencyId: Long) : NotFoundException("currency", currencyId)
