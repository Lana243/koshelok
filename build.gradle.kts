import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.7.2"
    id("io.gitlab.arturbosch.detekt") version "1.21.0"
    kotlin("jvm") version "1.6.21"
    kotlin("plugin.spring") version "1.6.21"
    kotlin("plugin.jpa") version "1.6.21"
    id("com.bmuschko.docker-spring-boot-application") version "8.0.0"
    jacoco
}

group = "sirius.tinkoff"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_17

repositories {
    mavenCentral()
}

dependencies {
    implementation(platform("org.springframework.cloud:spring-cloud-dependencies:2021.0.3"))
    implementation(platform("org.springframework.boot:spring-boot-dependencies:2.7.2"))
    implementation(platform("org.testcontainers:testcontainers-bom:1.17.3"))
    implementation("org.springdoc:springdoc-openapi-ui:1.6.9")

    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-validation")

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.testcontainers:postgresql")
    implementation("org.projectlombok:lombok:1.18.20")
    runtimeOnly("org.postgresql:postgresql")
    testImplementation("org.springframework.boot:spring-boot-starter-test")

    detektPlugins("io.gitlab.arturbosch.detekt:detekt-formatting:1.21.0")

    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(module = "junit")
        exclude(module = "mockito-core")
    }
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
    testImplementation("com.ninja-squad:springmockk:3.0.1")
    implementation ("io.github.microutils:kotlin-logging-jvm:2.1.20")


}

tasks {
    withType<KotlinCompile>().configureEach {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = "17"
        }
    }

    withType<Test>().configureEach() {
        useJUnitPlatform()
    }

    jacocoTestCoverageVerification {
        violationRules {
            rule {
                limit {
                    minimum = 0.6.toBigDecimal()
                }
            }
        }
        dependsOn(test)
    }

    jacocoTestReport {
        reports {
            html.required.set(true)
            xml.required.set(true)
        }
        dependsOn(test)
    }

    check {
        dependsOn(jacocoTestReport)
        dependsOn(jacocoTestCoverageVerification)
    }
}

detekt {
    toolVersion = "1.21.0"
    config = files("config/detekt/detekt.yml")
    buildUponDefaultConfig = true
    baseline = file("config/detekt/detekt-baseline.xml")
    autoCorrect = true
}

docker {
    springBootApplication {
        baseImage.set("eclipse-temurin:17-jre")
        images.add("registry.gitlab.com/Lana243/koshelok/koshelok:$version")
    }
    registryCredentials {
        url.set("registry.gitlab.com")
        username.set("gitlab-ci-token")
        password.set(System.getenv("CI_BUILD_TOKEN"))
    }
}
